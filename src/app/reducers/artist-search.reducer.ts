import { ArtistSearched } from "../models/artist-search.model";
import * as ArtistActions from "../actions/artist-search.actions"

const initialState: ArtistSearched = {
    id: 'Initial Id',
    name: 'Initial Name'
}

export function reducer(state: ArtistSearched[] = [initialState], action: ArtistActions.Actions) {
    switch(action.type) {
        case ArtistActions.ADD_ARTIST:
            return [...state, action.payload];
        default:
            return state;
    }
}