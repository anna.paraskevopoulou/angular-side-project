import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit, OnChanges{
  @Input() public albumName: string = null;
  @Input() public albumImage: string = null;
  @Input() public albumId: string = null;
  @Input() public releaseDate: string = null;

  @Output() public onAlbumClick: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes) {
     
    }
  }

  ngOnInit(): void {
  }

  public albumClicked() {
    this.onAlbumClick.emit(this.albumId);
  }

}
