import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  urlParams = new URLSearchParams(window.location.search);
  accessToken = this.urlParams.get('accessToken');
  refreshToken = this.urlParams.get('refreshToken');
  
  private _authorizationKey = 'Bearer ' + this.accessToken;

  private _httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': this._authorizationKey
    })
  };

  constructor(
    private _http: HttpClient
  ) { } 

  getCurrentUser(): Observable<any> {
    let url = 'https://api.spotify.com/v1/me';
    return this._http.get(url, this._httpOptions)
  }

  getCategories() {
    let url = 'https://api.spotify.com/v1/browse/categories';
    return this._http.get(url, this._httpOptions)
  }

  getCurrentUserPlaylists() {
    let url = 'https://api.spotify.com/v1/me/playlists';
    return this._http.get(url, this._httpOptions);
  }

  searchArtist(searchTerm: string) {
    let url = 'https://api.spotify.com/v1/search?type=artist&limit=10&q='+searchTerm;
    return this._http.get(url, this._httpOptions).pipe(catchError(this.handleError));
  }

  getArtist(id: string) {
    let url = 'https://api.spotify.com/v1/artists/'+id;
    return this._http.get(url, this._httpOptions);
  }

  getAlbums(artistId: string) {
    let url = 'https://api.spotify.com/v1/artists/'+artistId+'/albums';
    return this._http.get(url, this._httpOptions);
  }

  getAlbumTracks(albumId: string) {
    let url = 'https://api.spotify.com/v1/albums/'+albumId+'/tracks';
    return this._http.get(url, this._httpOptions);
  }

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}

