import { ArtistSearched } from './models/artist-search.model';

export interface AppState {
  readonly artist: ArtistSearched[];
}