import { Component, OnInit } from '@angular/core';
import { Routes, ActivatedRoute, Params } from '@angular/router';
import { SpotifyService } from '../spotify.service';

import { Artist } from './artist';
import { Album } from '../album/album';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent implements OnInit {
  id: number = null;
  artist: any = null;
  albums: any = null;

  constructor(
    private _spotifyService: SpotifyService,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this._activatedRoute.params
      .subscribe(
        (params: Params) => {
          this.id = params['id'];
          this._spotifyService.getArtist(this.id.toString())
            .subscribe(artist => {
              this.artist = artist;
            })

          this._spotifyService.getAlbums(this.id.toString())
            .subscribe(albums => {
              let values = Object.values(albums);
              this.albums = values;
            })
        });
  }

  public albumClicked(id: string) {
    this._spotifyService.getAlbumTracks(id).subscribe();
  }
}


