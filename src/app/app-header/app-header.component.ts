import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { Categories, User } from '../spotify.interfaces';
import { SpotifyService } from '../spotify.service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit {
  public categories$: Observable<any> = null;
  public categoriesVM: Categories = null;

  urlParams = new URLSearchParams(window.location.search);
  accessToken = this.urlParams.get('accessToken');
  refreshToken = this.urlParams.get('refreshToken');

  public currentUser: User = null;
  public currentUser$: Observable<User> = null;

  constructor(
    private _spotifyService: SpotifyService
  ) {
    this.categories$ = this._spotifyService.getCategories();
    this.currentUser$ = this._spotifyService.getCurrentUser();
  }

  ngOnInit(): void {
  }
}
