import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SpotifyService } from '../spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public searchInput: FormControl = new FormControl();
  public searchResults: any[] = null;

  constructor(private _spotifyService: SpotifyService) { }

  ngOnInit(): void {
    this.searchInput.valueChanges
    .subscribe(input => this._spotifyService.searchArtist(input)
      .subscribe(
        result => {
          let values = Object.values(result);
          if (values.length !== 0) this.searchResults = values[0].items;
          else this.searchResults = null;
        },
        error => {
          this.searchResults = null;
        }
      ));
  }

}
