import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { SpotifyService } from './spotify.service';
import { AppHeaderComponent } from './app-header/app-header.component';
import { TopListsComponent } from './categories/top-lists/top-lists.component';
import { MoodComponent } from './categories/mood/mood.component';
import { SummerComponent } from './categories/summer/summer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ArtistComponent } from './artist/artist.component';
import { AlbumComponent } from './album/album.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './reducers/artist-search.reducer';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    TopListsComponent,
    MoodComponent,
    SummerComponent,
    ArtistComponent,
    AlbumComponent,
    HomeComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    StoreModule.forRoot({
      artist: reducer
    }),
    StoreModule.forRoot({}, {})
  ],
  providers: [SpotifyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
