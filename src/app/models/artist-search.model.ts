export interface ArtistSearched {
    id: string;
    name: string;
}