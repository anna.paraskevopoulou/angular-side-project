import { Component } from '@angular/core';
import { SpotifyService } from './spotify.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'music-app';
  private uri: string = null;
  private id: string = null;

  constructor(private _spotifyService: SpotifyService) {}

  ngOnInit() {
  }

  public getUserPlaylist() {
    this._spotifyService.getCurrentUserPlaylists().pipe(first()).subscribe(res => {
      this.uri = res[0].uri;
      this.id = res[0].id;
    });

  }

}

