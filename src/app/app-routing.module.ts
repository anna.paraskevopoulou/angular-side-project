import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArtistComponent } from './artist/artist.component';
import { MoodComponent } from './categories/mood/mood.component';
import { SummerComponent } from './categories/summer/summer.component';
import { TopListsComponent } from './categories/top-lists/top-lists.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'top-lists', component: TopListsComponent},
  { path: 'mood', component: MoodComponent},
  { path: 'summer', component: SummerComponent},
  { path: 'users-playlists', component: SummerComponent},
  { path: 'artist/:id', component: ArtistComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
