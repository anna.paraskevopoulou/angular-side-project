import { Action } from "@ngrx/store";
import { ArtistSearched } from "src/app/models/artist-search.model";


export const ADD_ARTIST = '[ARTIST] Add';

export class AddArtist implements Action {
    readonly type = ADD_ARTIST;
    
    constructor(public payload: ArtistSearched) {}
}

export type Actions = AddArtist;